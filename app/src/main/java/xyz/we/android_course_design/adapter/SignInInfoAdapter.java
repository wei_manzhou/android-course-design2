package xyz.we.android_course_design.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import xyz.we.android_course_design.R;
import xyz.we.android_course_design.model.SignInInfo;

public class SignInInfoAdapter extends ArrayAdapter<SignInInfo> {

    private int resourceId;

    public SignInInfoAdapter(@NonNull Context context, int resource, List<SignInInfo> datas) {
        super(context, resource, datas);
        this.resourceId = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        SignInInfo signInInfo = getItem(position);
        Log.d("EE", signInInfo.toString());
        View view = LayoutInflater.from(getContext()).inflate(this.resourceId, parent, false);

        TextView studentName = view.findViewById(R.id.username);
        TextView studentNumb = view.findViewById(R.id.user_number);

        // 将信息设置到控件中
        studentName.setText(signInInfo.getStudentName());
        studentNumb.setText(signInInfo.getStudentNumber());

        return view;
    }

}
