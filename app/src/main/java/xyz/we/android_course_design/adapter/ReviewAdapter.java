package xyz.we.android_course_design.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import xyz.we.android_course_design.R;
import xyz.we.android_course_design.model.Review;
import xyz.we.android_course_design.model.SignInInfo;

public class ReviewAdapter extends ArrayAdapter<Review> {

    private int resourceId;

    public ReviewAdapter(@NonNull Context context, int resource, List<Review> datas) {
        super(context, resource, datas);
        this.resourceId = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Review review = getItem(position);
        Log.d("EE", review.toString());
        View view = LayoutInflater.from(getContext()).inflate(this.resourceId, parent, false);

        TextView reviewUsername = view.findViewById(R.id.review_item_username);
        TextView reviewContent = view.findViewById(R.id.review_item_content);

        // 将信息设置到控件中
        reviewUsername.setText(review.getUsername());
        reviewContent.setText(review.getContent());

        return view;
    }


}
