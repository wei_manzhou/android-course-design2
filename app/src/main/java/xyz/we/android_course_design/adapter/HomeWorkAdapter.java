package xyz.we.android_course_design.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import xyz.we.android_course_design.R;
import xyz.we.android_course_design.model.HomeWork;
import xyz.we.android_course_design.model.SignInInfo;

public class HomeWorkAdapter extends ArrayAdapter<HomeWork> {

    private int resourceId;

    public HomeWorkAdapter(@NonNull Context context, int resource, List<HomeWork> datas) {
        super(context, resource, datas);
        this.resourceId = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        HomeWork homeWork = getItem(position);
        Log.d("EE", homeWork.toString());
        View view = LayoutInflater.from(getContext()).inflate(this.resourceId, parent, false);

        TextView homeworkDate = view.findViewById(R.id.homework_date);
        TextView homeworkTitle = view.findViewById(R.id.homework_title);
        TextView homeworkDescription = view.findViewById(R.id.homework_description);

        // 将信息设置到控件中
        homeworkDate.setText(homeWork.getDate().toString());
        homeworkTitle.setText(homeWork.getTitle());
        homeworkDescription.setText(homeWork.getDescription());

        return view;
    }




}
