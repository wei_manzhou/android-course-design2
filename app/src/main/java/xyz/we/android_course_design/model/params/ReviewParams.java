package xyz.we.android_course_design.model.params;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import xyz.we.android_course_design.model.pojo.Review;

/**
 * @author snowflake
 * @version v1.0
 * @package xyz.we.androidcoursedesignbackend.params
 * @title ReviewParams
 * @description
 * @date 2020/10/27 - 18:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewParams {

    private Long id;

    private Long userId;

    private Long homeWorkId;

    private Long reviewId;

    private Review review;

    private Token token;


}
