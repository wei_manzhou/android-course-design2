package xyz.we.android_course_design.model.params;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author snowflake
 * @version v1.0
 * @package xyz.we.androidcoursedesignbackend.params
 * @title SignInParams
 * @description
 * @date 2020/10/27 - 18:47
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignInParams {

    private Long id;

    private Long userId;

    private Long classId;

    private Long teacherId;

    private String date;

    private Token token;

}
