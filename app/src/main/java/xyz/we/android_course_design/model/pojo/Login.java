package xyz.we.android_course_design.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author snowflake
 * @version v1.0
 * @package xyz.we.androidcoursedesignbackend.pojo
 * @title Login
 * @description
 * @date 2020/10/27 - 17:58
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Login {

    private Long id;

    private Long userId;

    private Long classId;

    private Long teacherId;

    private Date date;

}
