package xyz.we.android_course_design.model.pojo;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author snowflake
 * @version v1.0
 * @package xyz.we.androidcoursedesignbackend.pojo
 * @title Review
 * @description
 * @date 2020/10/27 - 17:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Review {

    private Long id;

    private Long userId;

    private Long homeWorkId;

    private Date date;

    private String content;

}
