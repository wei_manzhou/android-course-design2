package xyz.we.android_course_design.model.params;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author snowflake
 * @version v1.0
 * @package xyz.we.androidcoursedesignbackend.params
 * @title HomeWorkParams
 * @description
 * @date 2020/10/27 - 17:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HomeWorkParams {

    private Long id;

    private Long userId;

    private String title;

    private String info;

    private String description;

    private Token token;

}
