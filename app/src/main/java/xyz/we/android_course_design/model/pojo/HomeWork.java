package xyz.we.android_course_design.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author snowflake
 * @version v1.0
 * @package xyz.we.androidcoursedesignbackend.pojo
 * @title HomeWork
 * @description
 * @date 2020/10/27 - 17:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HomeWork {

    private Long id;

    private Long userId;

    private String title;

    private String info;

    private String description;

}
