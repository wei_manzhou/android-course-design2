package xyz.we.android_course_design.model.params;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author snowflake
 * @version v1.0
 * @package xyz.we.androidcoursedesignbackend.params
 * @title Token
 * @description
 * @date 2020/10/27 - 17:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Token {

    private String token;

}
