package xyz.we.android_course_design.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author snowflake
 * @version v1.0
 * @package xyz.we.androidcoursedesignbackend.pojo
 * @title User
 * @description
 * @date 2020/10/27 - 16:54
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private Long id;

    private String username;

    private String password;

    private String userNumber;

    private String type;

    public boolean isStudent() {
        return type.equals("student");
    };

}
