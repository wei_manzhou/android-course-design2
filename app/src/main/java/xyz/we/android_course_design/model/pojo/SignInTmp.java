package xyz.we.android_course_design.model.pojo;

import lombok.*;

import java.util.Date;

/**
 * @author snowflake
 * @version v1.0
 * @package xyz.we.androidcoursedesignbackend.pojo
 * @title SignInTmp
 * @description
 * @date 2020/10/27 - 18:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SignInTmp {

    private Long id;

    private Long teacherId;

    private Long studentId;

    private Integer status;

    private Date date;

    private boolean isTimeOut;

}
