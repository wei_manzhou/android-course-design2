package xyz.we.android_course_design.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author snowflake
 * @version v1.0
 * @package xyz.we.androidcoursedesignbackend.pojo
 * @title UserHomeWork
 * @description
 * @date 2020/10/27 - 17:13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserHomeWork {

    private Long id;

    private Long userId;

    private Long homeWorkId;

}
