package xyz.we.android_course_design.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author snowflake
 * @version v1.0
 * @package xyz.we.androidcoursedesignbackend.pojo
 * @title Class
 * @description
 * @date 2020/10/27 - 17:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Class {

    private Long id;

    private String className;

}
