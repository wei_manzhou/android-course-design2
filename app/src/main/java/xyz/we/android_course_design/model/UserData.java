package xyz.we.android_course_design.model;

import android.app.Application;

/**
 *
 * 存储全局的用户信息
 *
 * 1. 用户名
 * 2. number
 * 3. 账户类型
 * 4. 账户登陆后从服务器获取的 token
 *
 */
public class UserData extends Application {

    private String username;

    private String number;

    private String type;

    private String token;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
