package xyz.we.android_course_design.ui.backupdetails;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import xyz.we.android_course_design.R;

public class BackUpDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back_up_details);
    }
}