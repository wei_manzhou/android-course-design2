package xyz.we.android_course_design.ui.student.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import xyz.we.android_course_design.R;
import xyz.we.android_course_design.ui.homework.HomeWorkDetailsActivity;
import xyz.we.android_course_design.ui.student.ui.signin.StudentSignInActivity;
import xyz.we.android_course_design.ui.userinfo.UserInfoActivity;

public class StudentMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_main);

        Button signInButton = findViewById(R.id.student_sign_in_button_for_activity);
        Button checkHomeWork = findViewById(R.id.student_check_homework_for_activity);

        signInButton.setOnClickListener(e -> {
            Intent intent = new Intent(this, StudentSignInActivity.class);
            startActivity(intent);
        });

        checkHomeWork.setOnClickListener(e -> {
            Intent intent = new Intent(this, HomeWorkDetailsActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // 四个参数的含义。1，group的id,2,item的id,3,是否排序，4，将要显示的内容
        menu.add(0, 1, 0, "个人中心");
        menu.add(0, 2, 0, "推出");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Intent intent = new Intent(this, UserInfoActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }

}