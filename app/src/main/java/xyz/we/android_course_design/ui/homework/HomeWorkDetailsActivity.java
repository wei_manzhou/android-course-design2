package xyz.we.android_course_design.ui.homework;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import xyz.we.android_course_design.R;
import xyz.we.android_course_design.adapter.HomeWorkAdapter;
import xyz.we.android_course_design.model.HomeWork;

public class HomeWorkDetailsActivity extends AppCompatActivity {

    private ListView homeworkListView;

    private List<HomeWork> homeWorkList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work_details);

        initData();

        HomeWorkAdapter homeWorkAdapter = new HomeWorkAdapter(HomeWorkDetailsActivity.this,
                R.layout.homework_details_item_layout, homeWorkList);

        homeworkListView = findViewById(R.id.homework_listview);
        homeworkListView.setAdapter(homeWorkAdapter);

        homeworkListView.setOnItemClickListener((parent, view, position, id) -> {
            HomeWork homeWork = homeWorkList.get(position);

            Intent intent = new Intent(this, HomeWorkItemDetailsActivity.class);
            intent.putExtra("homework", homeWork);
            startActivity(intent);

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //四个参数的含义。1，group的id,2,item的id,3,是否排序，4，将要显示的内容
        menu.add(0, 1, 0, "添加作业");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Toast.makeText(this, "添加作业面板", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeWorkAddActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    private void initData() {
        for (int i = 0; i < 10; i++) {
            HomeWork homeWork = new HomeWork();
            homeWork.setTitle("我是作业" + i);
            homeWork.setDate(new Date());
            homeWork.setDescription("我是作业的描述" + i);

            homeWorkList.add(homeWork);
        }
    }

}
