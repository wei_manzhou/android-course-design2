package xyz.we.android_course_design.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import xyz.we.android_course_design.R;
import xyz.we.android_course_design.ui.homework.HomeWorkDetailsActivity;
import xyz.we.android_course_design.ui.loggindetails.LoginDetailsActivity;
import xyz.we.android_course_design.ui.login.LoginActivity;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        Button loggingButton = root.findViewById(R.id.check_logging_button);
        Button homeworkButton = root.findViewById(R.id.check_homework_button);
        Button evaluateButton = root.findViewById(R.id.check_evaluate_button);
        Button backupButton = root.findViewById(R.id.check_backup_button);


        // 分别添加事件
        loggingButton.setOnClickListener(e -> {

            Intent intent = new Intent(getActivity(), LoginDetailsActivity.class);
            startActivity(intent);
        });
        // 作业情况
        homeworkButton.setOnClickListener(e -> {
            Intent intent = new Intent(getActivity(), HomeWorkDetailsActivity.class);
            startActivity(intent);
        });
//        // 查看评价
//        loggingButton.setOnClickListener(e -> {
//            Intent intent = new Intent(getActivity(), LoginActivity.class);
//            startActivity(intent);
//        });
//        // 分别添加事件
//        loggingButton.setOnClickListener(e -> {
//            Intent intent = new Intent(getActivity(), LoginActivity.class);
//            startActivity(intent);
//        });

        return root;
    }

}
