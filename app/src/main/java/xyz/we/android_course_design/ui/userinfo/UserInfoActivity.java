package xyz.we.android_course_design.ui.userinfo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import xyz.we.android_course_design.R;

public class UserInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
    }
}