package xyz.we.android_course_design.ui.loggindetails;

import android.util.Log;
import android.util.Patterns;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.we.android_course_design.model.ClassName;
import xyz.we.android_course_design.model.SignInInfo;

public class LoginDetailsViewModel extends ViewModel {

    private String TAG = "EE";


    private MutableLiveData<Map<String, SignInInfo>> signInfo;


    public void getLogginDetails() {
        // TODO 获取登陆信息，根据默认条件，当前事件和空班名，则默认为第一个班的名称
//        return getLogginDetails(LocalDate.now().toString(), "");
    }

    public void getLogginDetails(String date, ClassName className) {

    }

    public void getLogginDetails(String date, String className) {

        // 发送 http 请求
        new Thread(() -> {

            OkHttpClient http = new OkHttpClient();

            RequestBody requestBody = new FormBody.Builder()
                    .add("date", date)
                    .add("class_name", className)
                    .build();

            Request request = new Request.Builder()
                    .url("http://localhost:8080/loggin_details")
                    .post(requestBody)
                    .build();

            try {
                Response response = http.newCall(request).execute();
                assert response.body() != null;
                String result = response.body().string();

                Log.d(TAG, "RESULT:" + result);

                // TODO 这里需要 json 数据转换

                // TODO 这里如果登陆成功可以跳转到主页面，并将用户信息存储到全局数据库
                // 如果用户名不存在提示用户存在，如果密码不正确提示用户密码不正确

            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }).start();
    }


}