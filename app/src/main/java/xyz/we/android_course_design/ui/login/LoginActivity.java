package xyz.we.android_course_design.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import xyz.we.android_course_design.MainActivity;
import xyz.we.android_course_design.R;
import xyz.we.android_course_design.model.response.LogInResponse;
import xyz.we.android_course_design.ui.student.ui.home.StudentMainActivity;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final RadioGroup radioGroup = findViewById(R.id.user_type_radio);

        loginButton.setOnClickListener(e -> {

            Log.d("EE", "登陆按钮触发");

            // 获取用户名
            String username = usernameEditText.getText().toString();
            String password = passwordEditText.getText().toString();
            int userTypeId = radioGroup.getCheckedRadioButtonId();

            loginViewModel.login(username, password,
                    R.id.user_type_student == userTypeId ? "student" : "teacher");

            LogInResponse logInResponse = loginViewModel.getLogInResponseMutableLiveData()
                    .getValue();
            if (logInResponse != null) {
                if (logInResponse.getUser() != null) {
                    Toast.makeText(this, "登陆成功", Toast.LENGTH_SHORT).show();
                    if (R.id.user_type_student == userTypeId) {
                        Intent intent = new Intent(this, StudentMainActivity.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(this, MainActivity.class);
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(this, logInResponse.getMsg(), Toast.LENGTH_SHORT).show();
                }
            } else {

}


        });

    }

}
