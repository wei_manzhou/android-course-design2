package xyz.we.android_course_design.ui.homework;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import xyz.we.android_course_design.R;
import xyz.we.android_course_design.adapter.ReviewAdapter;
import xyz.we.android_course_design.model.HomeWork;
import xyz.we.android_course_design.model.Review;

public class HomeWorkItemDetailsActivity extends AppCompatActivity {

    List<Review> reviewList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work_item_details);

        initData();

        ReviewAdapter reviewAdapter = new ReviewAdapter(HomeWorkItemDetailsActivity.this,
                R.layout.review_layout, reviewList);

        ListView listView = findViewById(R.id.homework_item_review);
        listView.setAdapter(reviewAdapter);

        Button submitReview = findViewById(R.id.homework_item_submit_review_button);

        submitReview.setOnClickListener(e -> {
            // 获取评论信息
            String content = ((EditText) findViewById(R.id.homework_item_review_content)).getText().toString();

            // TODO 发送网络请求
            Toast.makeText(this, content, Toast.LENGTH_SHORT).show();
        });
    }

    private void initData() {
        for (int i = 0; i < 10; i++) {
            Review review = new Review();
            review.setUsername("我是作业" + i);
            review.setDate(new Date());
            review.setContent("我是作业的描述" + i);

            reviewList.add(review);
        }
    }

}