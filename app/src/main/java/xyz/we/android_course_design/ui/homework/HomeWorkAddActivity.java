package xyz.we.android_course_design.ui.homework;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import java.util.Date;

import xyz.we.android_course_design.R;
import xyz.we.android_course_design.model.HomeWork;

public class HomeWorkAddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work_add);

        Button addHomeWorkButton = findViewById(R.id.new_homework_submit);

        addHomeWorkButton.setOnClickListener(e -> {
            // 获取title
            String homeWorkTitle = ((EditText) findViewById(R.id.new_homework_title)).getText().toString();
            String homeWorkDescription = ((EditText) findViewById(R.id.new_homework_description)).getText().toString();

            HomeWork homeWork = new HomeWork();
            homeWork.setTitle(homeWorkTitle);
            homeWork.setDescription(homeWorkDescription);
            homeWork.setDate(new Date());

            // TODO 需要发送网络请求添加信息

        });
    }
}