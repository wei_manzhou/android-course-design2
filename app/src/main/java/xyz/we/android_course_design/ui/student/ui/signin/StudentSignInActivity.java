package xyz.we.android_course_design.ui.student.ui.signin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import xyz.we.android_course_design.R;

public class StudentSignInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_sign_in);

        Button signButton = findViewById(R.id.student_sign_in_button);

        signButton.setOnClickListener(view -> {
            // TODO 发送网络请求，进行签到
            Log.d("EE", "签到成功");
            Toast.makeText(this, "签到成功", Toast.LENGTH_LONG);
        });

    }

}