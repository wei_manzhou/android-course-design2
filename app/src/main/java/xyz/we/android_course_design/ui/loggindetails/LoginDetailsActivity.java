package xyz.we.android_course_design.ui.loggindetails;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import xyz.we.android_course_design.R;
import xyz.we.android_course_design.adapter.SignInInfoAdapter;
import xyz.we.android_course_design.model.ClassName;
import xyz.we.android_course_design.model.SignInInfo;

public class LoginDetailsActivity extends AppCompatActivity {

    private LoginDetailsViewModel loginDetailsViewModel;

    private static final String[] m = {"2017222班", "2017221班"};

    // 日期
    private EditText datePicker;
    // 班级编号
    private Spinner classNameSpinner;

    private ArrayAdapter<String> classNameAdapter;

    List<ClassName> classNames = new ArrayList<>();
    List<SignInInfo> listItem = new ArrayList<>();  // ListView的数据源，这里是一个HashMap的列表

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_details);

        loginDetailsViewModel = ViewModelProviders.of(this).get(LoginDetailsViewModel.class);

        loginDetailsViewModel.getLogginDetails();

        initData();

        // 初始化适配器
        SignInInfoAdapter signInInfoAdapter = new SignInInfoAdapter(LoginDetailsActivity.this,
                R.layout.sign_in_details_layout, listItem);

        ListView areadly_sign_in_container = findViewById(R.id.areadly_sign_in_container);
        ListView no_areadly_sign_in_container = findViewById(R.id.no_areadly_sign_in_container);

        areadly_sign_in_container.setAdapter(signInInfoAdapter);
        no_areadly_sign_in_container.setAdapter(signInInfoAdapter);

        datePicker = findViewById(R.id.signin_date);
        datePicker.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                showDatePickDlg();
                return true;
            }
            return false;
        });
        datePicker.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                showDatePickDlg();
            } else {
                loginDetailsViewModel.getLogginDetails(String.valueOf(datePicker.getText()),
                        classNames.get(classNameSpinner.getSelectedItemPosition()));
            }
        });

        classNameSpinner = findViewById(R.id.signin_class_name);
        classNameAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, m);
        //设置下拉列表的风格
        classNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //将adapter 添加到spinner中
        classNameSpinner.setAdapter(classNameAdapter);
        //添加事件Spinner事件监听
        classNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // TODO 发送网络请求
                loginDetailsViewModel.getLogginDetails(String.valueOf(datePicker.getText()),
                        classNameAdapter.getItem(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }

        });
        //设置默认值
        classNameSpinner.setVisibility(View.VISIBLE);

    }

    protected void showDatePickDlg() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                LoginDetailsActivity.this,
                (view, year, monthOfYear, dayOfMonth) ->
                        LoginDetailsActivity.this.datePicker.setText(year + "-" + monthOfYear + "-" + dayOfMonth),
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        Log.d("EE", "初始化数据开始");
        for (int i = 0; i < 10; i++) {
            SignInInfo signInInfo = new SignInInfo();
            signInInfo.setStudentName("10111" + i);
            signInInfo.setStudentNumber("11111" + i);
            listItem.add(signInInfo);
        }
        Log.d("EE", "初始化数据完成");
    }

}