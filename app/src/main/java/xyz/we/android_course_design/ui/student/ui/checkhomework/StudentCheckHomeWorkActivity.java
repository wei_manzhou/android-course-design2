package xyz.we.android_course_design.ui.student.ui.checkhomework;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import xyz.we.android_course_design.R;
import xyz.we.android_course_design.adapter.HomeWorkAdapter;
import xyz.we.android_course_design.model.HomeWork;
import xyz.we.android_course_design.ui.homework.HomeWorkAddActivity;
import xyz.we.android_course_design.ui.homework.HomeWorkDetailsActivity;
import xyz.we.android_course_design.ui.homework.HomeWorkItemDetailsActivity;

public class StudentCheckHomeWorkActivity extends AppCompatActivity {

    private ListView homeworkListView;

    private List<HomeWork> homeWorkList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_check_home_work);

        initData();

        HomeWorkAdapter homeWorkAdapter = new HomeWorkAdapter(StudentCheckHomeWorkActivity.this,
                R.layout.homework_details_item_layout, homeWorkList);

        homeworkListView = findViewById(R.id.homework_listview);
        homeworkListView.setAdapter(homeWorkAdapter);

        homeworkListView.setOnItemClickListener((parent, view, position, id) -> {
            HomeWork homeWork = homeWorkList.get(position);

            Intent intent = new Intent(this, HomeWorkItemDetailsActivity.class);
            intent.putExtra("homework", homeWork);
            startActivity(intent);

        });
    }

    private void initData() {
        for (int i = 0; i < 10; i++) {
            HomeWork homeWork = new HomeWork();
            homeWork.setTitle("我是作业" + i);
            homeWork.setDate(new Date());
            homeWork.setDescription("我是作业的描述" + i);

            homeWorkList.add(homeWork);
        }
    }


}