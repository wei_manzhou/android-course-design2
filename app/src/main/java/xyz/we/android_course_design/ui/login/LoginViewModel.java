package xyz.we.android_course_design.ui.login;

import android.util.Log;
import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xyz.we.android_course_design.model.pojo.User;
import xyz.we.android_course_design.model.response.LogInResponse;

public class LoginViewModel extends ViewModel {

    private String TAG = "EE";

    private MutableLiveData<LogInResponse> logInResponseMutableLiveData = new MutableLiveData<>();

    public void login(String username, String password, String type) {
        User user;
        new Thread(() -> {
            // 发送 http 请求
            OkHttpClient http = new OkHttpClient();

            JSONObject json = new JSONObject();
            try {
                json.put("username", username);
                json.put("password", password);
                json.put("type", type);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = FormBody.create(MediaType.parse("application/json"), json.toString());

            Request request = new Request.Builder()
                    .url("http://192.168.1.103:8080/login")
                    .post(requestBody)
                    .build();

            try {
                Response response = http.newCall(request).execute();
                assert response.body() != null;
                String result = response.body().string();

                Log.d(TAG, "RESULT:" + result);

                Gson resultJSON = new Gson();
                LogInResponse logInResponse = resultJSON.fromJson(result, LogInResponse.class);

                System.out.println(logInResponse.toString());
                Log.d(TAG, "RESULT:" + logInResponse.toString());

                // TODO 这里如果登陆成功可以跳转到主页面，并将用户信息存储到全局数据库
                // 如果用户名不存在提示用户存在，如果密码不正确提示用户密码不正确
                logInResponseMutableLiveData.postValue(logInResponse);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }).start();

    }


    public LiveData<LogInResponse> getLogInResponseMutableLiveData() {
        return logInResponseMutableLiveData;
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }

}
